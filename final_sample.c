/*
*  dictionary.c
*  Author : Persevierance Musaka
*  
* Functions for Dictionary 
* A collection of mappings from WORDs to DESCRIPTIONs
* A WORD is a sequence of characters up to MAX_WORD_SIZE in length
* A DESCRIPTION is a sequence of characters (including spaces) up to
*   MAX_DESC_SIZE in length
*/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "dictionary.h"

struct dictionary_node{
    char word[MAX_WORD_SIZE], meaning[MAX_DESC_SIZE];
    struct dictionary_node *Left_Node, *Right_Node;
};

struct dictionary_node *dictionary_root = NULL;

void d_initialise(){
/**
* d_initialise: initialise the dictionary so that it contains no entries
*/
    dictionary_root = NULL;
}
//=============================================================================
struct dictionary_node * create_node(char *word, char *meaning){
    struct dictionary_node *new_node;
    new_node = (struct dictionary_node *)malloc(sizeof(struct dictionary_node));
    strcpy(new_node->word, word);
    strcpy(new_node->meaning, meaning);
    new_node->Left_Node = NULL;
    new_node->Right_Node = NULL;
    return new_node;
}
//=============================================================================
void insert(char *word, char *meaning){
    struct dictionary_node *parent = NULL, *current = NULL, *new_node = NULL;
    int res = 0;
    if(!dictionary_root){
        dictionary_root = create_node(word, meaning);
        return;
    }
    for (current  = dictionary_root; current != NULL;
        current = (res > 0) ? current->Right_Node : current->Left_Node)
    {
            res = strcasecmp(word, current->word);
            if (res == 0){
                printf("Word Exists\n");
                return;
            }
            parent = current;
        }

        new_node = create_node(word,meaning);
        res > 0 ? (parent->Right_Node = new_node) : (parent->Left_Node = new_node);
        return;
}
//=============================================================================
int d_read_from_file(const char * filename){
/* * Returns:     true (1) if the file was successfully imported into the
*                 dictionary;
*                 false (0) if the file was not successfully imported.
*/

    FILE *fp;
    char buffer[1000];
    char new_word[30];
    char new_meaning[250];

    fp = fopen(filename, "r");
    if(!fp){
        return 0;
    }
    else{
        while(fscanf(fp,"%s %[^\n]",new_word, new_meaning)!=EOF){
            if (new_word[0] != '.'){
                char * word = strdup(new_word);
                char * meaning = strdup(new_meaning);
                insert (word,meaning);
                /*printf("WORD \t\t:: %s \n",new_word);
                printf("MEANING \t:: %s\n", new_meaning);
                printf("WORD COPY \t:: %s \n",new_word);
                printf("MEANING COPY\t:: %s\n", new_meaning);
                printf("//================================//\n");*/
            }
        }
        return 1;
        fclose(fp);
    }

}

//=============================================================================
int d_lookup(const char * word, char * meaning){
/**
* d_lookup:    Looks up a word in the dictionary and returns the
*              description in the user-supplied character buffer.
* Returns:     true (1) if the word was found in the dictionary;
*              false (0) if the word was not found in the dictionary.
*/


    struct dictionary_node *temp = NULL;
    int res = 0;

    if(dictionary_root == NULL){
        return 0;
    }
    temp = dictionary_root;
    while(temp){
        if (strcasecmp(temp->word, word) == 0){
            printf("MEANING :: %s \n", temp->meaning);
            return 1;
            break;
        }
        temp = (strcasecmp(temp->word, word) > 0) ? temp->Left_Node : temp->Right_Node;
    } return 0;
}

//=============================================================================
  void inorderTraversal(struct dictionary_node *myNode) {
        if (myNode) {
                inorderTraversal(myNode->Left_Node);
                printf("Word    : %s\n", myNode->word);
                printf("Meaning : %s\n", myNode->meaning);
                printf("-----------------------------------------------------------------\n");
                inorderTraversal(myNode->Right_Node);
        }
        return;
  }


//============================================================================

int main(int argc, char ** argv)
{
    int i;
    char word[MAX_WORD_SIZE+1];
    char answer[MAX_DESC_SIZE+1];


    d_initialise();

    for (i=1; i<argc; i++)
        d_read_from_file(argv[i]);

    inorderTraversal(dictionary_root);

    // ascending_order(root);
    scanf("%s",word);
    while(word[0] != '.') {
        if (d_lookup(word,answer))
           printf("%s:\t%s\n", word, answer);
        else
           printf("%s:\t%s\n", word, "Not in dictionary");
        scanf("%s",word);
    }

    // get_word(word);
}