#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define MAX_WORD_SIZE   40
#define MAX_DESC_SIZE  200

typedef struct dictionary{
	char *word;
    char *meaning;
	struct dictionary *Left_Node;
	struct dictionary *Right_Node;
} Dictionary; 

Dictionary * root;

void d_initialise(){
	root = NULL;
}


Dictionary * create_Node(Dictionary * node, char * new_word, char * new_meaning){
    
    if(node == NULL){
        node = (Dictionary *) malloc (sizeof(Dictionary));

        node -> word = new_word;
        node -> meaning = new_meaning;
        node -> Left_Node = NULL;
        node -> Right_Node = NULL;

        return node;

    }
    else
    {
        int comp_val = strcmp(new_word, node -> word);

        if(comp_val < 0){
            node -> Left_Node = create_Node(node -> Left_Node, new_word, new_meaning);
        }

        else if(comp_val > 0){

            node -> Right_Node = create_Node(node -> Right_Node, new_word, new_meaning);
        }

        else{
            node -> meaning = new_meaning;
        }

    }

    return node;
}


int d_read_from_file(const char * filename){
    FILE *fp;
    char buffer[1000];
    char * meaning;

    fp = fopen(filename, "r");
    if (!fp)
        return 1;

        while ((fgets(buffer,1000,fp)!=NULL) && buffer[0] != '.'){
        // printf("%s", buffer);

        strtok_r(buffer, " ", &meaning);

        char * word = strdup(buffer);

        root = create_Node(root, word, meaning);

    }

        fclose(fp);
        return 0;    
}




void traverse_node(Dictionary * node){ 

    if(node != NULL){

        traverse_node(node -> Left_Node);
        printf("%s \n", (node -> word));
        traverse_node(node -> Right_Node);
    }
}


void traverse_tree(){   
    traverse_node(root);
}



char * tree_lookup(Dictionary * node, const char * s_word)
{
    int comp = strcmp(s_word, node -> word);
    char * f_meaning = NULL;

    if(node != NULL)
    {
        if(comp < 0)
        {
            f_meaning = tree_lookup(node -> Left_Node, s_word);
        }

        else if (comp > 0)
        {
            f_meaning = tree_lookup(node -> Right_Node, s_word);
        }

        else if (comp == 0)
        {
            f_meaning = node -> meaning;
        }
    }

    return f_meaning;
}

int d_lookup(const char * word, char * meaning){

    char * f_meaning = NULL;

    f_meaning = tree_lookup(root, word);

    printf("%s", f_meaning);

    if(f_meaning != NULL)
    {
        return 1;
    }
    else
    {
        return 0;
    }

}


int main(int argc, char ** argv){
    int i;
    char word[MAX_WORD_SIZE+1];
    char answer[MAX_DESC_SIZE+1];


    d_initialise();
    for (i=1; i<argc; i++)
        d_read_from_file(argv[i]);

    traverse_tree();

    scanf("%s",word);

    while(word[0] != '.') {
        if (d_lookup(word,answer))
           printf("%s:\t%s\n", word, answer);
        else
           printf("%s:\t%s\n", word, "Not in dictionary");
        scanf("%s",word);
    }
}
