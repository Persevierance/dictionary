/*
*  dictionary.c
*  Author : Persevierance Musaka
*  
* Functions for Dictionary 
* A collection of mappings from WORDs to DESCRIPTIONs
* A WORD is a sequence of characters up to MAX_WORD_SIZE in length
* A DESCRIPTION is a sequence of characters (including spaces) up to
*   MAX_DESC_SIZE in length
*/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "dictionary.h"

struct dictionary_node{                                           
    /*
    *  Define the Struct for a single node.
    *  struct can contain two charecter  members [word] and [meaning]
    *  it can also contain struct that contains left and right members.
    */
    char word[MAX_WORD_SIZE], meaning[MAX_DESC_SIZE];
    struct dictionary_node *Left_Node, *Right_Node;
};

struct dictionary_node *dictionary_root = NULL;

void d_initialise(){
/**
* d_initialise: initialise the dictionary so that it contains no entries
*/
    dictionary_root = NULL;
}
//=============================================================================
/*
 *  Create a new single node of the dictionary with parameters pointing to the word and meaning
 */
struct dictionary_node * create_node(char *word, char *meaning){
    struct dictionary_node *new_node;
    new_node = (struct dictionary_node *)malloc(sizeof(struct dictionary_node));
    strcpy(new_node->word, word);
    strcpy(new_node->meaning, meaning);
    new_node->Left_Node = NULL;
    new_node->Right_Node = NULL;
    return new_node;
}
//=============================================================================
void insert(char *word, char *meaning){
    /*
    * function to insert the word and the meaning into the node 
    * it checks it word if either smaller and insert it in the left node or
    * if it is larger and inserts it to the right node.
    * if the words are similar it checks meaning and if the meanings are 
    * different the second meaning overrides the first one 
    * a message if also send to the screen if a word has been replaced
    */

    struct dictionary_node *parent = NULL, *current = NULL, *new_node = NULL;
    int res = 0;
    if(!dictionary_root){
        dictionary_root = create_node(word, meaning);
        return;
    }
    for (current  = dictionary_root; current != NULL;
        current = (res > 0) ? current->Right_Node : current->Left_Node)
    {
            res = strcasecmp(word, current->word);
            if (res == 0){
                if (strcasecmp(meaning,current->meaning) != 0) {
                    strcpy(current->meaning, meaning);
                    printf("\n\n>>>>>>>>>>%s [MEANING REPLACED]<<<<<<<<<<\n\n", current->word);
                }
                return;
            }
            parent = current;
        }
        
        new_node = create_node(word,meaning);
        res > 0 ? (parent->Right_Node = new_node) : (parent->Left_Node = new_node);
        return;
}
//=============================================================================
void alphabetic_order_dict(struct dictionary_node *node) {
    /*
    *  Inorder Treverse the dictionary in alphabetic order each node 
    *  prints the word and the meaning in alphabetic order
    */
    if (node) {
        // printf("", );
        alphabetic_order_dict(node->Left_Node);
        printf("[ %s ]:: %s\n", node->word, node->meaning);
        alphabetic_order_dict(node->Right_Node);

    }
    return;
}

void a_order(){ 
/*
* Call the alphabetic_oder_dict on the root node and then check all nodes as it goes up the tree
* i.e. order the all nodes from the root
*/
alphabetic_order_dict(dictionary_root);
}
//============================================================================

int d_read_from_file(const char * filename){
/* * Returns:     true (1) if the file was successfully imported into the
*                 dictionary;
*                 false (0) if the file was not successfully imported.
*/

    FILE *fp;
    char buffer[1000];
    char new_word[30];
    char new_meaning[250];

    fp = fopen(filename, "r");
    if(!fp){
        return 0;
    }
    else{
        while(fscanf(fp,"%s %[^\n]",new_word, new_meaning)!=EOF){
            if (new_word[0] != '.'){
                char * word = strdup(new_word);
                char * meaning = strdup(new_meaning);
                insert (word,meaning); //put the word and the meaning into a new_node
            }
        }        
        return 1;
        fclose(fp);
    }    
}

//=============================================================================
int d_lookup(const char * word, char * meaning){
/**
* d_lookup:    Looks up a word in the dictionary and returns the
*              description in the user-supplied character buffer.
* Returns:     true (1) if the word was found in the dictionary;
*              false (0) if the word was not found in the dictionary.
*/


    struct dictionary_node *temp = NULL;

    temp = dictionary_root;
    while(temp){
        if (strcasecmp(temp->word, word) == 0){
            printf("\nMEANING >> %s \n", temp->meaning);
            return 1;
            break;
        }
        else if(strcasecmp(temp->word, word) > 0){
            temp = temp->Left_Node;
        }
        else
            temp = temp->Right_Node;
    } return 0;
}

//============================================================================
