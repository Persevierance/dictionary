#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "dictionary.h"
//#include "dictionary.c"

int main(int argc, char ** argv)
{
    int i;
    char word[MAX_WORD_SIZE+1];
    char answer[MAX_DESC_SIZE+1];


     d_initialise();             

    for (i=1; i<argc; i++)
        d_read_from_file(argv[i]);

        printf("+-------------------------------------------------------+\n");
        /*
        * call the function [alphabetic_order_dict] on the loaded dictionary
        * alphabetic ordered dictionary text files will be printed to the screen
        */
        a_order(); 
        printf("+-------------------------------------------------------+\n");
        printf("|\tDictionary Loaded  and Alphabetic Ordered\t|\n");
        printf("+-------------------------------------------------------+\n");

        printf("LOOK FOR : ");          //ask user what they are looking for 
     scanf("%s",word);                  //Read in the input from user keyboard 
     while(word[0] != '.') {            //check if the input is not '.'
         if (d_lookup(word,answer))     //call function d_lookup on the entered input
            printf("\t%s\n", answer);   //return meaning of the word from the dictionary if it exists
         else
            printf("%s:\t%s\n", word, "Not in dictionary"); //Tell user the word isn not in dictionary
         printf("LOOK FOR : ");         //ask the user again for a word to look for
         scanf("%s",word);              //Read in the input from user keyboard 
     }
}


